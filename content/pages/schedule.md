---
title: "Schedule"
date: 2022-10-14T13:33:32-04:00
draft: false
---

## CURRENT SCHEDULE

**CONNECTIONS ATLANTA**  
December 8th
Renaissance Atlanta Midtown Hotel  
866 W Peachtree St NW, Atlanta, GA 30308  
<br>

All times in **EASTERN**  
**9:30 am** - Exhibitor arrival and table setup To be completed by 11:00am  
**11:00 am** - Doors open to guests for portfolio/reel viewing  
**5:00 pm** - Wine tasting & creative mingling during portfolio/reel viewing  
(your artists are welcome to attend)  
**7:00 pm** - Exhibitor breakdown  
<br>

Lanyard color codes:  
**Black:** Visitors  
**Blue:** Jury  
**Red:** Exhibitors  
**White:** LeBook staff  
**Orange:** Artists represented by exhibiting agents; production services  
<br>

### WELCOME AND SETUP
Starting at 9:30 am, the CONNECTIONS team will accompany you to your table and begin serving complimentary breakfast. We encourage you to use this time to set up your space and familiarize yourself with the venue and fellow exhibitors.
Due to time constraints, your area must be set up by 11:00 am.  
<br>
Please remember to provide us with the names, emails & cell phone numbers of all staff attending the event by emailing derek.lebook@gmail.com.  
<br>
We will give a name tag to each staff member upon arrival. Only three team members will be allowed in the showroom at any time.  
<br>
From 11:00 am to 7:00 pm, creative professionals (including creative directors, art directors, art producers, brand managers, & other decision-makers) will attend CONNECTIONS to meet and view the talent you represent and the services you offer.  
<br>
Each exhibitor will have a space consisting of a 6-foot presentation table, 4 chairs, 1 power strip, 1 waste basket, and 1 easel.  
<br>
Due to the request of many visitors and space limitations, we strongly encourage the use of agency books and tablets such as iPads allowing a quick overview of your agency/company. If you want to bring anything more than an agency book, up to 4 portfolios, and iPads, you must request approval from LeBook before the show. You may rent a monitor from our A/V partner.
Exhibitor areas will need to be cleared by 8:00 pm.
<br>

### INVITATIONS
The success of CONNECTIONS is partly due to a very stringent guest selection. You may encourage guests to register, but we want to remind you that access to the show is limited to professionals and decision-makers from the creative industry and guests who have pre-registered on the registration site and received confirmation from LeBook at www.lebook.com/CONNECTIONS.
Please send us your guest list, considering the above criteria, as soon as possible.
Only the artists you represent may attend but will not be allowed to enter the show until 5:00 pm.
<br>

## UPCOMING EVENTS
### 2023

* Barcelona : February 9th
* San Francisco – March 14 
* Los Angeles – March 16 
* Paris : March 16th
* Miami – end of March
* Amsterdam : April 20th
* New York – June
* Milan : June 15th
* Berlin : July 6th
* Digital Connections: July
* Geneva : September 21st
* Chicago – End of September
* Minneapolis– end of September
* Stockholm : October 19th
* Atlanta – Beginning of November
* London : November 16th
* Mexico City: Early December
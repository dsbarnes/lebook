---
title: "Creative Submissions"
date: 2022-10-14T13:17:39-04:00
draft: true
---

CONNECTIONS will have a prestigious jury in attendance. On average, you will see 15 jurors throughout your CONNECTIONS day. Jurors will have ~20 minutes to view your nominated projects. Have your work ready to show, with headphones available for video. We suggest playing your videos from a local file on your computer.  
<br>
For more information concerning the creative awards, please see the Creative Awards Tech Specs or contact derek.lebook@gmail.com.


## OVERVIEW OF CREATIVE SUBMISSIONS
Your creative submissions and the jury are the most fundamental components of your CONNECTIONS experience.
If you feel you do not understand the creative submissions or award process, please reach out to production at derek.lebook@gmail.com immediately.  
<br>

At CONNECTIONS, we do our best to get you 15 meetings with leading decision-makers from the most respected companies in the industry. We will have an invite-only jury who will come to you throughout the day to review your creative submissions. Each meeting lasts approximately 20 minutes, but you should expect some variance. You will use your creative submissions to showcase your work and process during this time.  
<br>

LeBook will place the awards on our website and use them to promote our exhibitors and jurors on social media. Winners will receive extra promotion through email, social media, and a winners page on our website.  
<br>

### SUBMISSION CATEGORIES
Below is the complete list of categories. Choose one per award:  
<br>

PRINT ADVERTISING   
PRINT EDITORIAL   
PRINT PERSONAL  
VIDEO ADVERTISING   
VIDEO EDITORIAL   
VIDEO PERSONAL  
POST-PRODUCTION VFX/CGI   
POST-PRODUCTION RETOUCHING  
TECHNOLOGY & NON-TRADITIONAL AR/VR/MR   
TECHNOLOGY & NON-TRADITIONAL EXPERIENTIAL   
TECHNOLOGY & NON-TRADITIONAL SOCIAL  
PRODUCTION ADVERTISING   
PRODUCTION EDITORIAL PRODUCTION EVENT  
AUDIO ADVERTISING   
AUDIO PERSONAL  
<br>

## REQUIRED ASSETS AND DEADLINES
Use this form when entering creative submissions (Please only submit twice):
https://forms.gle/Fr8GUzNkfo67QYWn6  
<br>

For video submissions, provide a Vimeo or Youtube Link to the video. If you would like a custom thumbnail, please provide that as well. Previously we accepted raw video files. However, our website does not like them, and we are now only accepting links to videos.  
<br>

For print submissions, please provide a download link for up to 5 images, a max of 10MB, ideally in PNG format, but any standard format will work.  
<br>

For both video and print submissions, provide the work’s title, category, and description of up to 160 words. Credits will be placed within the description if provided.  
<br>

### DEADLINES
The deadline for creative submissions is **NOVEMBER 17th.**
It is imperative to the quality of the show and your experience that you meet the deadlines for the creative submissions. Missing them will eliminate you from multiple marketing opportunities and leave a poor first impression on the judges (because you will not get appropriately listed in the jury book).
If you feel you are in danger of missing a creative submission deadline, reach out to Derek at derek. lebook@gmail.com to make arrangements.
 
## JUDGING CRITERIA
Originality of concept  
Distinctive Vision  
Authenticity  
Innovation  
Technical Excellency  
Overall production  
<br>

## ANNOUNCING WINNERS
We announce winners within two weeks of the end of the show.  
An email announcement to the exhibitors and our partners will be sent out as well as various social media posts highlighting the winners within each category.
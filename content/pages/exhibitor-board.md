---
title: "Exhibitor Board"
date: 2022-10-14T13:17:49-04:00
draft: true
---

## WHAT IS THE EXHIBITOR BOARD
The exhibitor board is how you will display who you are to the jury, other exhibitors, and attendees. You may use a single image, a collage, or something more creative like a QR code. You may be as creative as you like within the constraints of our template (see the following page for examples).
Each board will have the name of the Exhibitor, or an artist you would like to promote, a list of services, locations, and up to two websites.
<br>

## REQUIRED ASSETS AND DEADLINE
You have **2 WEEKS** to submit your exhibitor board assets after signing your contract,
but no later than **November 17th.**  

Provide the required information using this link:  
https://forms.gle/FbzGdW2jc24TW1K3A  
<br>

**Image file**
CMYK colorspace !important  
Any format (that supports CMYK, which is very important)  
Dimensions of 19 inch height x 23.5 inch width Maximum size of 250MB  
<br>

**Text**
Name of exhibitor or artist you would like to promote List of Services - 80 character max  
Locations serviced - 80 character max  
Website - 2 site max  
